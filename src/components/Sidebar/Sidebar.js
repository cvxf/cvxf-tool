/*!

=========================================================
* Light Bootstrap Dashboard React - v2.0.1
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import { useLocation, NavLink } from "react-router-dom";

import { Nav } from "react-bootstrap";

function Sidebar({ color, image, routes }) {
  const location = useLocation();
  const activeRoute = (routeName) => {
    return location.pathname.indexOf(routeName) > -1 ? "active" : "";
  };
  return (
    <div className="sidebar" data-image={image} data-color={color}>
      <div
        className="sidebar-background"
        style={{
          backgroundImage: "url(" + image + ")",
        }}
      />
      <div className="sidebar-wrapper">
        <div className="logo d-flex align-items-center justify-content-center">
          <a href="/" className="simple-text logo-mini mx-1">
            <div className="logo-img">
              <svg
                version="1.0"
                xmlns="http://www.w3.org/2000/svg"
                width="48.000000pt"
                height="48.000000pt"
                viewBox="0 0 128.000000 128.000000"
                preserveAspectRatio="xMidYMid meet"
              >
                <g
                  transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)"
                  fill="#7f33ff"
                  stroke="none"
                >
                  <path d="M152 818 c-5 -7 -12 -42 -14 -77 -4 -50 -9 -68 -26 -82 -28 -22 -28 -36 -1 -58 17 -14 22 -32 26 -92 6 -82 14 -99 49 -99 30 0 33 35 4 43 -18 4 -20 14 -20 73 0 49 -5 72 -17 86 -15 17 -15 19 0 36 12 13 17 37 17 76 0 49 3 59 20 63 29 8 26 43 -4 43 -13 0 -28 -6 -34 -12z" />
                  <path d="M950 810 c-12 -12 -20 -33 -20 -55 0 -28 -4 -35 -20 -35 -13 0 -20 -7 -20 -20 0 -13 7 -20 20 -20 19 0 20 -7 20 -95 0 -88 1 -95 20 -95 19 0 20 7 20 95 0 95 0 95 25 95 18 0 25 5 25 20 0 15 -7 20 -26 20 -23 0 -25 3 -22 33 3 28 7 32 36 35 52 5 36 42 -19 42 -10 0 -28 -9 -39 -20z" />
                  <path d="M1060 811 c0 -10 9 -21 20 -24 17 -4 20 -14 20 -65 0 -42 5 -65 16 -76 14 -14 14 -18 0 -38 -11 -15 -16 -45 -16 -86 0 -55 -3 -65 -20 -69 -29 -8 -26 -43 4 -43 36 0 49 24 54 106 4 59 9 78 23 87 24 15 24 43 0 50 -15 4 -19 17 -23 76 -3 39 -11 78 -18 86 -18 22 -60 20 -60 -4z" />
                  <path d="M315 711 c-85 -35 -83 -191 3 -215 49 -14 96 3 100 37 l3 27 -28 -17 c-64 -36 -116 14 -93 89 13 46 49 60 92 35 27 -16 28 -16 28 9 0 36 -56 55 -105 35z" />
                  <path d="M450 711 c0 -4 18 -55 39 -112 34 -90 42 -104 63 -107 23 -3 28 5 63 100 21 57 40 109 43 116 3 10 -2 13 -18 10 -19 -2 -28 -17 -51 -87 -16 -46 -30 -86 -32 -88 -3 -2 -19 37 -37 87 -25 70 -37 90 -51 90 -11 0 -19 -4 -19 -9z" />
                  <path d="M680 709 c0 -6 14 -31 31 -55 l31 -45 -36 -55 c-20 -29 -36 -56 -36 -59 0 -3 9 -5 20 -5 13 0 30 16 49 46 l29 47 27 -47 c17 -29 34 -46 46 -46 26 0 24 14 -11 65 -16 24 -30 47 -30 52 0 4 14 28 30 53 35 52 36 60 12 60 -9 0 -28 -18 -42 -40 -14 -22 -27 -40 -31 -40 -3 0 -15 18 -28 40 -14 25 -29 40 -42 40 -10 0 -19 -5 -19 -11z" />
                </g>
              </svg>
            </div>
          </a>
        </div>
        <Nav>
          {routes.map((prop, key) => {
            if (!prop.redirect)
              return (
                <li
                  className={
                    prop.upgrade
                      ? "active active-pro"
                      : activeRoute(prop.layout + prop.path)
                  }
                  key={key}
                >
                  <NavLink to={prop.layout + prop.path} className="nav-link">
                    <i className={prop.icon} />
                    <p>{prop.name}</p>
                  </NavLink>
                </li>
              );
            return null;
          })}
        </Nav>
      </div>
    </div>
  );
}

export default Sidebar;
