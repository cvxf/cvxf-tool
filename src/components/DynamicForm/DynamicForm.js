import React, { useState } from "react";
import { Container, Form, Button, Row, Col } from "react-bootstrap";

const renderFormGroup = (key, schema, formData, setFormData) => {
  const handleChange = (e) => {
    setFormData({
      ...formData,
      [key]: e.target.value,
    });
  };

  return (
    <Form.Group as={Row} key={key}>
      <Form.Label column sm="2">
        {key}
      </Form.Label>
      <Col sm="10">
        <Form.Control
          type={schema.type === "string" ? "text" : schema.type}
          value={formData[key]}
          onChange={handleChange}
          required={schema.required}
        />
      </Col>
    </Form.Group>
  );
};

const DynamicForm = ({ schema }) => {
  const [formData, setFormData] = useState({});

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(formData);
  };

  return (
    <Container>
      <h1>Dynamic Form</h1>
      <Form onSubmit={handleSubmit}>
        {Object.keys(schema.properties).map((key) =>
          renderFormGroup(key, schema.properties[key], formData, setFormData)
        )}
        <Button type="submit">Submit</Button>
      </Form>
    </Container>
  );
};

export default DynamicForm;
