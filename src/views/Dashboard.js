import React from "react";

// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";

import { default as JSForm } from "@rjsf/core";

// import { RJSFSchema } from '@rjsf/utils';
import validator from "@rjsf/validator-ajv8";

import schema from "../assets/json/resume_schema.json";

function Dashboard() {
  const log = (type) => console.log.bind(console, type);

  return (
    <>
      <Container fluid>
        <Row>
          <Col lg="12" sm="12">
            <h1>shuncs</h1>
            <JSForm
              schema={schema}
              validator={validator}
              onChange={log("changed")}
              onSubmit={log("submitted")}
              onError={log("errors")}
            />
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Dashboard;
