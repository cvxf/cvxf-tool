import React, { useState } from "react";
import "./App.css";
import Toast from "react-bootstrap/Toast";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

const ExampleToast = ({ children }) => {
  const [show, toggleShow] = useState(true);

  return (
    <>
      {!show && <Button onClick={() => toggleShow(true)}>Show Toast</Button>}
      <Toast show={show} onClose={() => toggleShow(false)}>
        <Toast.Header>
          <strong className="mr-auto">React-Bootstrap</strong>
        </Toast.Header>
        <Toast.Body>{children}</Toast.Body>
      </Toast>
    </>
  );
};

function App() {
  return (
    <Container fluid className="bg-secondary p-0">
      <Row>
        <Col></Col>
        <Col>
          <Container fluid className="p-3 m-0 text-white bg-dark rounded-3">
            <h1 className="header">Welcome To React-Bootstrap</h1>
            <ExampleToast>
              We now have Toasts
              <span role="img" aria-label="tada">
                🎉
              </span>
            </ExampleToast>
          </Container>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
