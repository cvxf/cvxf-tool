/*!

=========================================================
* Light Bootstrap Dashboard React - v2.0.1
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom/client";

import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/animate.min.css";
import "./assets/scss/light-bootstrap-dashboard-react.scss?v=2.0.0";
import "./assets/css/demo.css";
import "@fortawesome/fontawesome-free/css/all.min.css";

import AdminLayout from "./layouts/Admin.js";

import routes from "./routes.js";

const root = ReactDOM.createRoot(document.getElementById("root"));

const getRoutes = (routes) => {
  return routes.map((prop, key) => {
    console.debug("YIP");
    if (prop.layout === "/admin") {
      console.debug("yep");
      return (
        <Route
          path={prop.layout + prop.path}
          element={prop.element}
          key={key}
        />
      );
    } else {
      return null;
    }
  });
};

root.render(
  <BrowserRouter>
    <Routes>
      <Route path="/admin/" element={<AdminLayout />}>
        {getRoutes(routes)}
      </Route>
      <Route path="/" element={<Navigate to="/admin/dashboard" replace />} />
    </Routes>
  </BrowserRouter>
);
